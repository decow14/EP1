#include <string>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include "imagem.hpp"
#include "pgm.hpp"
#include "ppm.hpp"
using namespace std;

int main(int argc, char ** argv) {
  int menu;
  do{
  cout<<"Digite o tipo:\n"<<"1 - Pgm\n"<<"2 - Ppm\n"<<"0 - fechar"<<endl;
  cin>>menu;
  switch(menu){
        case 0:
              cout<<"Sair";
              break;
        case 1:
            cout<<"Pgm"<<endl;
            Pgm *pgm;
            pgm=new Pgm();
            pgm->digiteEndereco();//endereco da imagem
            pgm->abrirImagemLeitura();//abrir essa imagem
            pgm->armazenarPixel();
            pgm->pegarNumeroMagico();//numero do arquivo
            pgm->testeNumeroMagico();//teste desse numero
            pgm->pegarPosicaoInicial();
            pgm->pegarAltura();
            pgm->pegarLargura();
            pgm->calculaTamanhoTotal();
            pgm->pegarValorMaximo();
            pgm->fecharImagem();
            delete (pgm);
        break;
        case 2:
            cout<<"Ppm"<<endl;
            Ppm *ppm;
            ppm=new Ppm();
            ppm->digiteEndereco();//endereco da imagem
            ppm->abrirImagemLeitura();//abrir essa imagem
            ppm->armazenarPixel();
            ppm->pegarNumeroMagico();//numero do arquivo
            ppm->testeNumeroMagico();//teste desse numero
            ppm->pegarAltura();
            ppm->pegarLargura();
            ppm->calculaTamanhoTotal();
            ppm->pegarValorMaximo();
            ppm->fecharImagem();
            delete (ppm);
        break;
        default:
            cout<<"Opção inválida"<<endl;
        break;
      }
      cout<<endl;
  }while(menu!=0);
};
